#!/usr/bin/python

# Copyright (C) 2008-2011 Stefano Zacchiroli <zack@debian.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# Last-Modified:    Wed, 07 May 2008 16:46:12 +0200

import datetime
import os.path
import re
import string
import sys
import json

from debian import deb822
from glob import glob
from genshi.template import TemplateLoader

vcss = ['svn', 'git', 'bzr', 'darcs', 'cvs', 'hg', 'arch', 'mtn']
bindir = '/home/zack/debian/debian-stats/bin'
sources = glob('/home/zack/debian/apt/sources/unstable/*')
outdir = '/home/zack/www/www.upsilon.cc/static/vcs-usage'

def get_vcs_stats(fnames):
    """ return a dictionary mapping VCS name to the number of packages using it
    """
    stats = {}
    total = 0
    using_vcs = 0
    for vcs in vcss:
        stats[vcs] = 0
    for sources in fnames:
        f = file(sources)
        for stanza in deb822.Sources.iter_paragraphs(f):
            total += 1
            for vcs in vcss:
                if stanza.has_key('vcs-' + vcs):
                    stats[vcs] += 1
                    using_vcs += 1
                    break
        f.close()
    return (stats, total, using_vcs)

def render_stats((stats, total, using_vcs)):
    loader = TemplateLoader([ bindir ])
    tmpl = loader.load('vcs-usage.genshi')
    vcss = stats.keys()
    vcss.sort()
    stream = tmpl.generate(stats=stats, total=total, using_vcs=using_vcs,
            vcss=vcss, timestamp=datetime.datetime.now())
    html = file(os.path.join(outdir, 'index.html'), 'w')
    for chunk in stream.serialize():
        html.write(chunk)
    html.close()

def make_json((stats, total, using_vcs)):
    value = stats
    value['total'] = total
    value['using_vcs'] = using_vcs
    output = {'values': [value]}
    with open(os.path.join(outdir, 'usage.json'), 'w') as f:
        json.dump(output, f)


stats = get_vcs_stats(sources)
render_stats(stats)
make_json(stats)

