#!/usr/bin/python

# Copyright (C) 2007-2011 Stefano Zacchiroli <zack@debian.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

import re
import sys
from debian import deb822

old_homepage_RE = re.compile(r'^\s*Homepage\s*:?', re.IGNORECASE)
look_for = sys.argv[1]
for fname in sys.argv[2:]:
    f = file(fname)
    for stanza in deb822.Packages.iter_paragraphs(f):
        if look_for == "old":
            dsc_lines = stanza['description'].split('\n')
            if filter(old_homepage_RE.match, dsc_lines):
                print stanza['package']
        elif look_for == "new":
            if stanza.has_key('homepage'):
                print stanza['package']
    f.close()
