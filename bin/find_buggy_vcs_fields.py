#!/usr/bin/python

# Copyright (C) 2007-2011 Stefano Zacchiroli <zack@debian.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.

# Last-Modified:    Mon, 31 Dec 2007 11:19:47 +0100

import psycopg2
import re
import string
import sys

from debian import deb822
from subprocess import Popen, PIPE

dbconn = psycopg2.connect(
        database="svnbuildstat",
        host="svnbuildstat.debian.net",
        user="svnbuildstat",
        password="SuperZob")

def get_svnb_urls(dbconn):
    """ return a dictionary mapping (source) package names to vcs urls as known
    by svnbuildstat.debian.net """
    cursor = dbconn.cursor()
    cursor.execute('select name, uri from package')
    vcs_urls = {}
    while True:
        res = cursor.fetchone()
        if not res:
            break
        package, url = res
        if package and url:
            vcs_urls[package] = url
    return vcs_urls

def get_sources_urls(fnames):
    """ return a dictionary mapping (source) package names to vcs urls as
    listed in source stanzas """
    pkgs = {}
    for sources in fnames:
        f = file(sources)
        for stanza in deb822.Sources.iter_paragraphs(f):
            if stanza.has_key('vcs-svn'):
                pkgs[stanza['package']] = stanza['vcs-svn']
        f.close()
    return pkgs

def normalize_svn_url(url):
    url = url.rstrip('/')
    url = re.sub(r'svn\.debian\.org/svn/', 'svn.debian.org/', url, 1)
    return url

svnb_urls = get_svnb_urls(dbconn)
srcs_urls = get_sources_urls(sys.argv[1:])

for pkg, svnb_url in svnb_urls.iteritems():
    if not srcs_urls.has_key(pkg):
        print "%s\tMISSING_VCS_URL\t%s" % (pkg, svnb_url)
    else:
        svnb_url, src_url = map(normalize_svn_url, [svnb_url, srcs_urls[pkg]])
        if src_url != svnb_url:
            print "%s\tWRONG_VCS_URL\t%s\tvs\t%s" % (pkg, svnb_url, src_url)
        else:
            print "%s\tOK_VCS_URL\t%s" % (pkg, svnb_url)

